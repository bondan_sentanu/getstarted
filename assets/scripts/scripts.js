var getstarted = {    

    homepageInit: function() {
           
    },

    domManipulation: function() {
             
    },    

    initCarousel: function() {                

        function getGridSize() {
            return (window.innerWidth < 767) ? 1 : 3;
          }         

        $('.flexslider').flexslider({
          animation: "slide",
          animationLoop: false,
          itemWidth: 340,      
          itemMargin: 0,
          minItems: getGridSize(), // use function to pull in initial value
          maxItems: getGridSize(), // use function to pull in initial value
          start: function(slider){            
            flexslider = slider;
          }
        });
        
        $('.flexslider-hero').flexslider();
    },

    eventCollection: function () {                    

    },

    init: function() {
        this.homepageInit();        
        this.eventCollection();
        this.initCarousel();
    }
}

$(document).ready(function() {
    getstarted.init();

    $(window).resize(function() {
        var gridSize = (window.innerWidth < 767) ? 1 : 3;
     
        flexslider.vars.minItems = gridSize;
        flexslider.vars.maxItems = gridSize;
    });
});
