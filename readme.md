README.md

FILE CREATED BY : bondan_sentanu@yahoo.com for getstarted.com.au
DATE CREATED: May 6th. 2016

this website is built with these technology:
1. html 5
2. jquery 1.11 (minimum)
3. bootstrap3 css framework
4. flexslider jquery plugin for the carousel.

remaining:
- minor style in blog carousel (prev-next buttons)
- tweak in responsive style (menu, slideshow of hero, font size etc)